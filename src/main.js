'use strict';

function update_fps() {
    fps = parseInt(document.getElementById("slider_fps").value);
    document.getElementById("label_fps_value").innerHTML = fps.toFixed(0);
}

function showImage(index) {
    document.getElementById("img" + index).style = "visibility: visible"
}

function hideImage(index) {
    document.getElementById("img" + index).style = "visibility: hidden"
}

function updateSeed(seed) {
    for (let i = 0; i < 18; ++i) {
        let url = "https://thisanimedoesnotexist.ai/results/psi-" + (0.3 + 0.1 * i).toFixed(1) + "/seed" + seed + ".png"
        document.getElementById("img" + i).setAttribute("src", url);
    }
}


let current_image = 0;
let fps = 30;

function animation_loop() {
    hideImage(current_image);
    current_image = (current_image + 1) % 18;
    showImage(current_image);
    setTimeout(() => requestAnimationFrame(animation_loop), 1000 / fps);
};

function update_loop() {
    update_fps();
    requestAnimationFrame(update_loop);
}

function main() {
    document.getElementById("button_seed_input").addEventListener(
        "click",
        () => updateSeed(document.getElementById("field_seed").value),
    )
    requestAnimationFrame(update_loop);
    requestAnimationFrame(animation_loop);
}

window.onload = main;
